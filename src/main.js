import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import vueAwesomeCountdown from 'vue-awesome-countdown'
import VueApexCharts from 'vue-apexcharts'



Vue.config.productionTip = false

Vue.use(vueAwesomeCountdown, 'vac')
Vue.use(VueApexCharts);

Vue.prototype.$navBarname = 'My App'

Vue.component('apexchart', VueApexCharts)

export const GestureEventBus = new Vue();


new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
