import Vue from 'vue'
import VueRouter from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import VocabularyList from '@/components/VocabularyList'
import Learning from '@/components/Learning'
import Test from '@/components/Test'
import Translate from '@/components/Translate'
import LearningSummary from  '@/components/LearningSummary'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HelloWorld
  },
  {
    path: '/vocabularylist',
    name: 'Vocabulary List',
    component: VocabularyList
  },
  {
    path: '/learning/:word',
    name: 'Learning',
    component: Learning
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/translate',
    name: 'Real-time Translate',
    component: Translate
  },
  {
    path: '/learningsummary',
    name: 'Learning Summary',
    component:LearningSummary
  }
]

const router = new VueRouter({
  routes
})

router.afterEach((to) => {
  if(to.name == 'Learning'){
      Vue.prototype.$navBarname = "Expression";
  }else{
    Vue.prototype.$navBarname = to.name;
  }

})

export default router
